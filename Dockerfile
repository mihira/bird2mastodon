FROM python:3.6-buster AS base

#Install twint
WORKDIR /root

RUN git clone --depth=1 https://github.com/DeepCISO/twint.git && \
	cd /root/twint && \
	pip3 install . -r requirements.txt

#Move to run python script
WORKDIR /usr/src/app

COPY ./src/. .

FROM base AS test
RUN python3 -m unittest

FROM base AS build
