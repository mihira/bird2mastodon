#!/usr/bin/env python

from urllib.parse import urlparse
import requests
from functools import partial
import re

URL_PATTERN = re.compile(r'https?://\S+')
AT_PATTERN = re.compile(r'((?<!\S)(@[^ \t\n\r\f\v@]+))(?!@)(?=\s)')
SESSION = requests.Session()

def _twitter2nitter(url):
    return url.replace('twitter.com', 'nitter.net')

def _tco2url(tco):
    response = SESSION.get(tco)
    return response.url

def tco2nitter(url):
    parsed_url = urlparse(url)
    if parsed_url.netloc == 't.co':
        url = _tco2url(url)
        parsed_url = urlparse(url)
    if 'twitter.com' in parsed_url.netloc:
        return _twitter2nitter(url)
    return url

def _match2url(match):
    return tco2nitter(match.group(0))

def tweet2toot(tweet):
    toot = URL_PATTERN.sub(partial(_match2url), tweet)
    toot = AT_PATTERN.sub(r'\1@twitter.com', toot)
    return toot