#!/usr/bin/env python

import twint
import os

DATA_PATH = os.environ['DATA_PATH']

def usernames():
    return os.environ['USERNAMES'].split(',')

def scrape(config, username):
    print('Scraping', username)
    config.Username = username
    config.Output = DATA_PATH + username + ".csv"
    config.Resume = DATA_PATH + username + ".id"
    # Run
    twint.run.Search(config)

if __name__ == '__main__':
    # Configure
    config = twint.Config()
    config.Limit = int(os.environ['TWINT_LIMIT'])
    config.Store_csv = True
    config.Profile_full = True
    config.User_full = True
    config.Database = DATA_PATH + 'scraped.db'
    for username in usernames():
        scrape(config, username)
    print('End')
