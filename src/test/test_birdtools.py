#!/usr/bin/env python

import unittest

from utils.birdtools import tco2nitter, tweet2toot

class BirdToolsTestCase(unittest.TestCase):
    def test_tco_to_normal_url(self):
        self.assertEqual(tco2nitter('https://t.co/vQ7wDWJM4V'), 'https://joinfediverse.wiki/Fediverse_flyer')

    def test_tco_to_nitter_url(self):
        self.assertEqual(tco2nitter('https://t.co/KBjXVyaaKV'), 'https://nitter.net/joinmastodon/status/1519167440311263233/photo/1')

    def test_twitter_to_nitter_url(self):
        self.assertEqual(tco2nitter('https://twitter.com/joinmastodon/status/1514573864944209924'), 'https://nitter.net/joinmastodon/status/1514573864944209924')

    def test_any_url_to_any_url(self):
        self.assertEqual(tco2nitter('https://mihira.me'), 'https://mihira.me')

    def test_tweet_to_toot_simple(self):
        self.assertEqual(tweet2toot('This is a simple text!'), 'This is a simple text!')

    def test_tweet_to_toot_with_tco_url(self):
        self.assertEqual(tweet2toot('Look at this flyer: https://t.co/vQ7wDWJM4V it is great!'), 'Look at this flyer: https://joinfediverse.wiki/Fediverse_flyer it is great!')

    def test_tweet_to_toot_with_at(self):
        self.assertEqual(tweet2toot('Hey @joinmastodon I am looking to join'), 'Hey @joinmastodon@twitter.com I am looking to join')

    def test_tweet_to_toot_with_invalid_at(self):
        self.assertEqual(tweet2toot('This was @typed@very@wrong !!!'), 'This was @typed@very@wrong !!!')

    def test_tweet_to_toot_with_at_and_email(self):
        self.assertEqual(tweet2toot('Hey @joinmastodon this is my mail: mail@mihira.me'), 'Hey @joinmastodon@twitter.com this is my mail: mail@mihira.me')

if __name__ == '__main__':
    unittest.main()