# bird2mastodon

Copy tweets from a Twitter account to a Mastodon mirror

## Docker containers
### bird2csv
Container to scrape tweets from users and store it into csv file (for now)

### Executing
```bash
docker-compose build
docker-compose run bird2csv #run script
docker-compose run bird2csv bash #navigate inside container files
```